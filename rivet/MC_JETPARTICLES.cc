// -*- C++ -*-
#include "Rivet/Analysis.hh"
#include "Rivet/Projections/FinalState.hh"
#include "Rivet/Projections/FastJets.hh"
#include "Rivet/Projections/DressedLeptons.hh"
#include "Rivet/Projections/MissingMomentum.hh"
#include "Rivet/Projections/DirectFinalState.hh"
#include <fstream>

namespace Rivet {


  /// Select interesting jets and write out their constituents' properties
  class MC_JETPARTICLES : public Analysis {
  public:

    /// Constructor
    RIVET_DEFAULT_ANALYSIS_CTOR(MC_JETPARTICLES);


    /// @name Analysis methods
    /// @{

    /// Initialise projections and output before the run
    void init() {

      // Get jets, leptons, MET
      const FinalState fs(Cuts::abseta < 4.9);
      declare(fs, "Particles");
      FastJets jetfs(fs, FastJets::ANTIKT, 1.0, JetAlg::Muons::NONE, JetAlg::Invisibles::NONE);
      declare(jetfs, "Jets");
      DirectFinalState bare_leps(Cuts::abspid == PID::MUON || Cuts::abspid == PID::ELECTRON);
      DressedLeptons dressed_leps(fs, bare_leps, 0.1, Cuts::abseta < 2.5 && Cuts::pT > 20*GeV);
      declare(dressed_leps, "Leptons");
      declare(MissingMomentum(fs), "MET");

      // Initialise CSV file
      _fout = std::ofstream(getOption("JETCSV", getEnvParam<string>("RIVET_JETCSV", "jetparticles.csv")));
      writefield("metpx", 5);
      writefield("metpy", 5);
      writefield("metpz", 5);
      writefield("npart", 5);
      writefield("pid,pt,eta,phi", 15, "");

    }


    /// Perform the per-event analysis
    void analyze(const Event& event) {

      // Retrieve objects
      Particles particles = apply<FinalState>(event, "Particles").particles();
      //Particles chparticles = filter_select(particles, isCharged);
      Particles leptons = apply<FinalState>(event, "Leptons").particlesByPt();
      if (filter_select(leptons, Cuts::pT > 100*GeV).size() > 0) vetoEvent;
      //Particles elecs = filter_select(leptons, isElectron);
      //Particles muons = filter_select(leptons, isMuon);
      Jets jets = apply<FastJets>(event, "Jets").jetsByPt(Cuts::pT > 50*GeV);
      idiscardIfAnyDeltaRLess(jets, leptons, 0.4);
      //Jets bjets = filter_select(jets, hasBTag(Cuts::pT > 5*GeV && Cuts::abseta < 2.5));
      P3 vmet = apply<MissingMomentum>(event, "MET").missingMom().p3(); //vectorPtMiss();

      // Write CSV row
      //if (jets.empty()) vetoEvent;
      if (jets.size() < 2) vetoEvent;
      //const Jet& j = jets[0];
      // Use *subleading* jet by preference for VLQs
      const Jet& j = jets[jets.size() > 1 ? 1 : 0];
      writefield(vmet.px(), 5);
      writefield(vmet.py(), 5);
      writefield(vmet.pz(), 5);
      writefield(j.size(), 5);
      for (size_t i = 0; i < j.size(); ++i) {
        const Particle& p = j.constituents()[i];
        writefield(p.pid(), 5);
        writefield(p.pT()/GeV, 7);
        writefield(p.eta(), 7);
        writefield(p.phi(), 7, ((i+1) < j.size() ? ", " : ""));
      }
    }


    /// Finalize (nothing to do since ofstream closes automatically)
    // void finalize() {}

    /// @}


    /// Helper method for writing columns in the CSV
    template <typename T>
    void writefield(const T& x, size_t width, const string& post=", ") {
      _fout.precision(width);
      _fout << std::setw(width) << x << post;
      if (post.empty()) _fout << endl;
    }


    /// CSV output file
    std::ofstream _fout;

  };


  RIVET_DECLARE_PLUGIN(MC_JETPARTICLES);

}

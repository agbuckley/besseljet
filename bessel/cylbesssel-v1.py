#! /usr/bin/env python3

"""\
%(prog)s <csvfile>

Compute Bessel function overlaps with standardised jet constituents
"""

## Parse command line
import argparse
ap = argparse.ArgumentParser(usage=__doc__)
ap.add_argument("CSV")
ap.add_argument("-t", "--tag", dest="TAG", default="", help="a tag (e.g. 'sig'/'bkg') to put in the output filenames")
ap.add_argument("-P", "--plotevts", dest="PLOTEVTS", type=int, nargs="+", default=[], help="make plots for the given event numbers")
ap.add_argument("-H", "--plothists", dest="PLOTHISTS", action="store_true", default=False, help="make histogram plots of the scores")
ap.add_argument("-v", "--verbose", dest="VERBOSE", action="store_true", default=False, help="enable debug printouts")
ap.add_argument("--nmax", dest="NMAX", type=int, default=3, help="Bessel n_max")
ap.add_argument("--mmax", dest="MMAX", type=int, default=3, help="Bessel m_max")
args = ap.parse_args()
if not args.TAG:
    args.TAG += "_"


import numpy as np
import scipy.special as sp
from scipy.special import spherical_jn
from scipy.special import jv
from scipy.special import jn, jn_zeros
from scipy.special import yn, yn_zeros
from scipy.special import jve
from scipy.optimize import curve_fit
from matplotlib import pyplot as plt
#from mpl_toolkits.mplot3d import Axes3D
#from matplotlib import cm


class Part:
    def __init__(self, pid, pt, eta, phi):
        self.pid = pid
        self.pt = pt
        self.eta = eta
        self.phi = phi

        self.jet = None
        self.rho = None
        self.angle = None

    @property
    def p3(self):
        return np.array([self.px, self.py, self.pz])

    @property
    def px(self):
        return self.pt * np.cos(self.phi)

    @property
    def py(self):
        return self.pt * np.sin(self.phi)

    @property
    def pz(self):
        tan_th2 = np.exp(-self.eta)
        tan_th = 2 * tan_th2 / (1 - tan_th2**2)
        return self.pt / tan_th

    @property
    def E(self):
        return np.sqrt( self.pt**2 + self.pz**2 )


class Jet:
    def __init__(self):
        self._p3 = None
        self.parts = []
        self.evtmet = np.array([0.0, 0.0])

    @property
    def nparts(self):
        return len(self.parts)

    @property
    def p3(self):
        if self._p3 is None:
            p3 = np.array([0.0, 0.0, 0.0])
            for p in self.parts:
                p3 += p.p3
            self._p3 = p3
        return self._p3

    @property
    def px(self):
        return self.p3[0]

    @property
    def py(self):
        return self.p3[1]

    @property
    def pz(self):
        return self.p3[2]

    @property
    def pt2(self):
        return self.px**2 + self.py**2

    @property
    def pt(self):
        return np.sqrt(self.pt2)

    @property
    def E(self):
        return np.sqrt(self.pt2 + self.pz**2)

    @property
    def eta(self):
        # tan2x = 2 tanx / (1 - tanx**2) => tan(th/2) = sin(th) / (1 + cos(th))
        # return -np.log( (self.pt/self.E) / (1 + (self.pz/self.E)) )
        return -np.log( self.pt / (self.E + self.pz) )

    @property
    def phi(self):
        return np.arctan2(self.py, self.px)



## Parse each line of the CSV file as a jet
jets = []
with open(args.CSV) as csvfile:
    first = True
    for line in csvfile:
        if first:
            first = False
            continue
        j = Jet()
        entries = [e.strip() for e in line.split(",")]
        j.evtmet = [float(entries[0]), float(entries[1])]
        nparts = int(entries[3])
        for i in range(nparts):
            j.parts.append( Part(int(entries[4*i+4]), float(entries[4*i+5]), float(entries[4*i+6]), float(entries[4*i+7])) )
        jets.append(j)

## Transform particles into coords relative to jet centroid
scores = {}
for ij, j in enumerate(jets):
    #if ij > 100: break
    if ij % 100 == 0:
        print(ij)

    ## Get relative coords for each constituent
    tparts = []  #np.zeros([j.parts, 2])
    for i, p in enumerate(j.parts):
        deta = p.eta - j.eta
        dphi = np.fmod(p.phi - j.phi + 3*np.pi, 2*np.pi) - np.pi
        if args.VERBOSE:
            print("  ", i, ":", p.eta, j.eta, "->", deta, ";", p.phi, j.phi, "->", dphi)
        tparts.append([p.pid, p.pt, deta, dphi])

    ## Construct and diagonalise momentum tensor
    tmom = np.zeros([2,2], dtype=float)
    for tp1 in tparts:
        for tp2 in tparts:
            tmom[0,0] += np.sqrt( abs( tp1[1]*(tp1[2]) * tp2[1]*(tp2[2]) ) )
            tmom[0,1] += np.sqrt( abs( tp1[1]*(tp1[2]) * tp2[1]*(tp2[3]) ) )
            tmom[1,0] += np.sqrt( abs( tp1[1]*(tp1[3]) * tp2[1]*(tp2[2]) ) )
            tmom[1,1] += np.sqrt( abs( tp1[1]*(tp1[3]) * tp2[1]*(tp2[3]) ) )
    evals, evecs = np.linalg.eig(tmom)
    #print("  ", evals, evecs, "", sep="\n")
    #print("  ", tmom, evecs @ tmom @ evecs.T, "", sep="\n")
    #print("  ", tmom, evecs.T @ tmom @ evecs, "", sep="\n")

    ## Jet display for debug
    def jetdisp(parts, vecs):
        from matplotlib import pyplot as plt
        plt.figure(figsize=(8,8))
        plt.axhline(color="lightgray")
        plt.axvline(color="lightgray")
        plt.arrow(0, 0, vecs[0,0], vecs[0,1], color="red", linestyle="-", width=0.02)
        plt.arrow(0, 0, vecs[1,0], vecs[1,1], color="blue", linestyle="-", width=0.02)
        plt.xlim(-1,1)
        plt.ylim(-1,1)
        plt.scatter([tp[2] for tp in parts], [tp[3] for tp in parts], [10*tp[1] for tp in parts])
        plt.tight_layout()
        plt.show()

    #jetdisp(tparts, evecs)

    ## Transform particle 2-vectors into diagonal basis
    dparts = []
    sumpm = 0.0
    for tp in tparts:
        xy = evecs @ tp[2:4]
        dparts.append([tp[0], tp[1], xy[0], xy[1]])
        sumpm += xy[0] * tp[1]
    #jetdisp(dparts, evecs.T @ evecs)

    ## Final reorientation to put most pT in the +x side
    if sumpm < 0:
        print(f"Reorienting: sum(x * pT) = {sumpm:2.1g}")
        for idp in range(dparts):
            dparts[i][2] *= -1
            dparts[i][3] *= -1

    ## Restructure data
    dparts_ar = np.array(dparts)
    etas, phis, pts = [], [], []
    for i in range(len(dparts_ar)): #number of particles in each event
        etas.append(dparts_ar[i][2])
        phis.append(dparts_ar[i][3])
        pts.append(dparts_ar[i][1])
    sumpt = sum(pts)

    # Cartesian in eta, phi to polar in r, theta
    def cart2pol(x, y):
        crho = np.sqrt(x**2 + y**2)
        cphi = np.arctan2(y, x)
        return(crho, cphi)

    # Pick off the mth zero of Bessel function Jn
    def displacement(n, m, r, theta):
        k = jn_zeros(n, args.MMAX+1)[m]
        return np.sin(n*theta) * jn(n, r*k)

    # TODO: Tune these
    for n in range(1,args.NMAX+1):
        for m in range(1,args.MMAX+1):
            #n, m = 3, 4

            if ij in args.PLOTEVTS:
                r = np.linspace(0, 1, 100)
                theta = np.linspace(0, 2 * np.pi, 100)
                x = np.array([rr*np.cos(theta) for rr in r])
                y = np.array([rr*np.sin(theta) for rr in r])
                z = np.array([displacement(n, m, rr, theta) for rr in r])
                plt.pcolor(x, y, z, alpha=0.3, rasterized=True) #, dpi=120)
                #plt.contour(x, y, z, alpha=0.2)
                for i in range(len(dparts_ar)):
                    plt.plot(dparts_ar[i][2], dparts_ar[i][3], "ro")
                plt.savefig(f"evt_{args.TAG}{ij:03d}_{n:d}{m:d}.pdf")
                #plt.show()
                plt.close()

            # Then evaluate the functions at constit point and calc score
            score = []
            for i in range(len(dparts_ar)):
                thisconstit = cart2pol(dparts_ar[i][2], dparts_ar[i][3])
                thisz = displacement(n, m, thisconstit[0], thisconstit[1])
                score.append( dparts_ar[i][2]*thisz/sumpt )
            totscore_nm = sum(score)
            scores.setdefault( (n,m), [] ).append(totscore_nm)

            if args.VERBOSE:
                print(f"Score[{n:d},{m:d}] for this jet: -> {totscore_nm:2.2f}")


## Write out score arrays for joint histogramming, and plots
print("\n\n")
print("Histogramming overlaps:")
for n in range(1,args.NMAX+1):
    for m in range(1,args.MMAX+1):
        print(f"{n:d},{m:d} -> {scores[(n,m)]}")
        np.savetxt(f"bessel_{args.TAG}{n:d}{m:d}.dat.gz", scores[(n,m)])
        if args.PLOTHISTS:
            plt.figure()
            plt.hist(scores[(n,m)])
            plt.plot([], linestyle="none", label=f"$({n:d},{m:d})$")
            plt.legend()
            plt.tight_layout()
            plt.savefig(f"bessel_{args.TAG}{n:d}{m:d}.pdf")
            plt.close()

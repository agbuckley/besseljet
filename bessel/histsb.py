#! /usr/bin/env python3

"""\
%(prog)s <datfile1> [<datfile2> ...]

Histogram and plot the observables in the given data files
"""

## Parse command line
import argparse
ap = argparse.ArgumentParser(usage=__doc__)
ap.add_argument("DATFILES", nargs="+")
ap.add_argument("-n", "--nbins", dest="NBINS", type=int, default=10, help="number of bins to use in the histograms")
ap.add_argument("-o", "--outname", dest="OUTNAME", default="bessel", help="output base name")
ap.add_argument("--names", dest="NAMES", default=None, help="comma-separated list of legend names for the inputs")
ap.add_argument("--title", dest="TITLE", default=None, help="plot title")
# ap.add_argument("--samebins", dest="SAMEBINS", action="store_true", default=False, help="use the same binning for all inputs")
ap.add_argument("-v", "--verbose", dest="VERBOSE", action="store_true", default=False, help="enable debug printouts")
args = ap.parse_args()
args.NAMES = args.NAMES.split() if args.NAMES else ["Sig", "Bkg"]

## Read data
import numpy as np
scores = [np.loadtxt(df) for df in args.DATFILES]

# TODO: implement common binning
from matplotlib import pyplot as plt
plt.figure()
for i, score in enumerate(scores):
    plt.hist(score, bins=args.NBINS)
    # kwargs = { "bins" : args.NBINS }
    # if args.NAMES:
    #     kwargs["label"] = args.NAMES[i]
    # plt.hist(score, **kwargs)
#plt.plot([], linestyle="none", label=f"$({n:d},{m:d})$")
if args.TITLE:
    plt.title(args.TITLE)
plt.legend()
plt.tight_layout()
plt.savefig(f"{args.OUTNAME}.pdf")
# plt.savefig(f"bessel_{args.TAG}{n:d}{m:d}.pdf")
plt.close()

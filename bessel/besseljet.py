#! /usr/bin/env python3

"""\
%(prog)s <csvfile>

Compute Bessel function overlaps with standardised jet constituents

TODO: convert inputs to HDF5?
"""

## Parse command line
import argparse
ap = argparse.ArgumentParser(usage=__doc__)
ap.add_argument("CSV")
args = ap.parse_args()


import numpy as np

class Part:
    def __init__(self, pid, pt, eta, phi):
        self.pid = pid
        self.pt = pt
        self.eta = eta
        self.phi = phi

        self.jet = None
        self.rho = None
        self.angle = None

    @property
    def p3(self):
        return np.array([self.px, self.py, self.pz])

    @property
    def px(self):
        return self.pt * np.cos(self.phi)

    @property
    def py(self):
        return self.pt * np.sin(self.phi)

    @property
    def pz(self):
        tan_th2 = np.exp(-self.eta)
        tan_th = 2 * tan_th2 / (1 - tan_th2**2)
        return self.pt / tan_th

    @property
    def E(self):
        return np.sqrt( self.pt**2 + self.pz**2 )


class Jet:
    def __init__(self):
        self._p3 = None
        self.parts = []
        self.evtmet = np.array([0.0, 0.0])

    @property
    def nparts(self):
        return len(self.parts)

    @property
    def p3(self):
        if self._p3 is None:
            p3 = np.array([0.0, 0.0, 0.0])
            for p in self.parts:
                p3 += p.p3
            self._p3 = p3
        return self._p3

    @property
    def px(self):
        return self.p3[0]

    @property
    def py(self):
        return self.p3[1]

    @property
    def pz(self):
        return self.p3[2]

    @property
    def pt2(self):
        return self.px**2 + self.py**2

    @property
    def pt(self):
        return np.sqrt(self.pt2)

    @property
    def E(self):
        return np.sqrt(self.pt2 + self.pz**2)

    @property
    def eta(self):
        # tan2x = 2 tanx / (1 - tanx**2) => tan(th/2) = sin(th) / (1 + cos(th))
        # return -np.log( (self.pt/self.E) / (1 + (self.pz/self.E)) )
        return -np.log( self.pt / (self.E + self.pz) )

    @property
    def phi(self):
        return np.arctan2(self.py, self.px)



## Parse each line of the CSV file as a jet
jets = []
with open(args.CSV) as csvfile:
    first = True
    for line in csvfile:
        if first:
            first = False
            continue
        j = Jet()
        entries = [e.strip() for e in line.split(",")]
        j.evtmet = [float(entries[0]), float(entries[1])]
        nparts = int(entries[2])
        for i in range(nparts):
            j.parts.append( Part(int(entries[4*i+3]), float(entries[4*i+4]), float(entries[4*i+5]), float(entries[4*i+6])) )
        jets.append(j)

## Transform particles into coords relative to jet centroid
for ij, j in enumerate(jets):
    if ij > 4: break
    print(ij)

    ## Get relative coords for each constituent
    tparts = []  #np.zeros([j.parts, 2])
    for i, p in enumerate(j.parts):
        deta = p.eta - j.eta
        dphi = np.fmod(p.phi - j.phi + 3*np.pi, 2*np.pi) - np.pi
        print("  ", i, ":", p.eta, j.eta, "->", deta, ";", p.phi, j.phi, "->", dphi)
        tparts.append([p.pid, p.pt, deta, dphi])

    ## Construct and diagonalise momentum tensor
    tmom = np.zeros([2,2], dtype=float)
    for tp1 in tparts:
        for tp2 in tparts:
            tmom[0,0] += np.sqrt( abs( tp1[1]*(tp1[2]) * tp2[1]*(tp2[2]) ) )
            tmom[0,1] += np.sqrt( abs( tp1[1]*(tp1[2]) * tp2[1]*(tp2[3]) ) )
            tmom[1,0] += np.sqrt( abs( tp1[1]*(tp1[3]) * tp2[1]*(tp2[2]) ) )
            tmom[1,1] += np.sqrt( abs( tp1[1]*(tp1[3]) * tp2[1]*(tp2[3]) ) )
    evals, evecs = np.linalg.eig(tmom)
    print("  ", evals, evecs, "", sep="\n")
    print("  ", tmom, evecs @ tmom @ evecs.T, "", sep="\n")
    print("  ", tmom, evecs.T @ tmom @ evecs, "", sep="\n")

    ## Jet display for debug
    def jetdisp(parts, vecs):
        from matplotlib import pyplot as plt
        plt.figure(figsize=(8,8))
        plt.axhline(color="lightgray")
        plt.axvline(color="lightgray")
        plt.arrow(0, 0, vecs[0,0], vecs[0,1], color="red", linestyle="-", width=0.02)
        plt.arrow(0, 0, vecs[1,0], vecs[1,1], color="blue", linestyle="-", width=0.02)
        plt.xlim(-1,1)
        plt.ylim(-1,1)
        plt.scatter([tp[2] for tp in parts], [tp[3] for tp in parts], [10*tp[1] for tp in parts])
        plt.tight_layout()
        plt.show()

    jetdisp(tparts, evecs)

    ## Transform particle 2-vectors into diagonal basis
    dparts = []
    for tp in tparts:
        xy = evecs @ tp[2:4]
        dparts.append([tp[0], tp[1], xy[0], xy[1]])
    jetdisp(dparts, evecs.T @ evecs)

    # TODO: align with larger sigma(long) in +1 direction
    # TODO: flip with larger sigma(short) in +2 direction

    # TODO: compute Bessel-function overlaps

    # TODO: plot Bessel coeffs

    # TODO: compute S / B discrimination

# BesselJet

A project to search for jet-substructure signals of deviation from QCD-like jets, with
occupancy gaps of a typical angular spectrum, using a Bessel-function basis on standardised
jet images.

## Structure

`rivet/`: a Rivet analysis to produce event summary data in CSV, corresponding to MET and jet vectors,
          and constituent info for each jet

`bessel/`: a NumPy-based computation of standardised jets and their Bessel coefficients (for several
           pT-exponents)

`analysis/`: statistical analysis of Bessel-coefficient spectra, for SM/BSM discrimination
